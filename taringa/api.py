import requests

from oauth import MyOAuth1

class TaringaAPI(object):
    def __init__(self, my_oauth):
        self._my_oauth = my_oauth

        self._baseUrl = "https://api.taringa.net/"

    def auth(self, signature_type):
        return self._my_oauth.auth(signature_type)

    def follow(self, user_id):
        r = requests.get(self._baseUrl + "user/follow/{0}".format(user_id),
                auth = self.auth("auth_header"))

        if r.status_code == 200:
            return r.json()
        else:
            r.raise_for_status()

    def like_shout(self, shout_id):
        r = requests.get(self._baseUrl + "shout/like/{0}".format(shout_id), auth=self.auth("auth_header"))

        if r.status_code == 200:
            return r.json()
        else:
            r.raise_for_status()

    def unlike_shout(self, shout_id):
        r = requests.get(self._baseUrl + "shout/unlike/{0}".format(shout_id), auth=self.auth("auth_header"))

        if r.status_code == 200:
            return r.json()
        else:
            r.raise_for_status()

    def get_my_info(self):
        r = requests.get(self._baseUrl + "user/view", auth = self.auth("auth_header"))

        if r.status_code == 200:
            return r.json()
        else:
            r.raise_for_status()

    def get_user_info(self, nickname):
        r = requests.get(self._baseUrl + "user/nick/view/{0}".format(nickname))

        if r.status_code == 200:
            return r.json()
        else:
            r.raise_for_status()

    def get_followings(self, user_id, page):
        r = requests.get(
                self._baseUrl + "user/followings/view/{0}?count=50&page={1}".format(user_id, page))

        if r.status_code == 200:
            return r.json()
        else:
            r.raise_for_status()
