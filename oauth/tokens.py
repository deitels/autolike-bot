import re

import requests
from requests_oauthlib import OAuth1Session

class UserOrPasswordInvalid(Exception):
    pass

def get_app_client_tokens():
    update_url = "https://github.com/deitels/tconsumer/blob/master/tconsumer.txt"

    r = requests.get(update_url)

    if r.status_code == 200:
        match = re.findall(":::(.+?):::(.+?):::", r.text)
        client_key, client_secret = match[0]
        return client_key, client_secret
    else:
        r.raise_for_status()

def get_oauth_client_session(client_key, client_secret):
    oauth_session = OAuth1Session(client_key=client_key,
                                  client_secret=client_secret)
    return oauth_session

def get_resource_owner_tokens(oauth_client_session):
    request_token_url = "https://api.taringa.net/oauth/request_token"
    fetch_response = oauth_client_session.fetch_request_token(request_token_url)

    resource_owner_key = fetch_response.get("oauth_token")
    resource_owner_secret = fetch_response.get("oauth_token_secret")

    return resource_owner_key, resource_owner_secret

def login(resource_owner_key, username, password):
    login_url = "https://www.taringa.net/registro/login-submit.php"

    data = {"form_type": "widget",
            "oauth_token": resource_owner_key,
            "dont_cookie": 1,
            "redirect": "authorize.php?oauth_token={0}".format(resource_owner_key),
            "nick": username,
            "pass": password}

    r = requests.post(login_url, data=data)

    if r.status_code == 200:
        json = r.json()
        if json["status"] != 0:
            return json
        else:
            raise UserOrPasswordInvalid()
    else:
        r.raise_for_status()

def get_callback(login_json):
    r = requests.head(
            "https://www.taringa.net/widgets2/{0}".format(login_json["data"]["redirect"]))

    if r.status_code == 302:
        return r.headers["location"]
    else:
        r.raise_for_status()

def get_verifier(oauth_client_session, resource_owner_key, username, password):
    login_json = login(resource_owner_key, username, password)
    redirect_response = get_callback(login_json)

    oauth_response = oauth_client_session.parse_authorization_response(
            redirect_response)

    verifier = oauth_response.get("oauth_verifier")

    return verifier

def get_access_tokens(client_key,
                      client_secret,
                      resource_owner_key,
                      resource_owner_secret,
                      verifier):
    access_token_url = "https://api.taringa.net/oauth/access_token"

    oauth = OAuth1Session(client_key = client_key,
                          client_secret = client_secret,
                          resource_owner_key = resource_owner_key,
                          resource_owner_secret = resource_owner_secret,
                          verifier = verifier)

    oauth_tokens = oauth.fetch_access_token(access_token_url)

    resource_owner_key = oauth_tokens.get("oauth_token")
    resource_owner_secret = oauth_tokens.get("oauth_token_secret")

    return resource_owner_key, resource_owner_secret

def get_tokens(username, password):
    client_key, client_secret = get_app_client_tokens()
    oauth_client_session = get_oauth_client_session(client_key, client_secret)
    temp_res_owner_key, temp_res_owner_secret = get_resource_owner_tokens(oauth_client_session)
    verifier = get_verifier(oauth_client_session, temp_res_owner_key, username, password)
    resource_owner_key, resource_owner_secret = get_access_tokens(client_key,
            client_secret, temp_res_owner_key,
            temp_res_owner_secret, verifier)

    return client_key, client_secret, resource_owner_key, resource_owner_secret
