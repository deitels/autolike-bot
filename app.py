import os
import time
import traceback

from taringa import Taringa, UserOrPasswordInvalid

username = os.environ["TARINGA_USERNAME"]
password = os.environ["TARINGA_PASSWORD"]

account = Taringa(username, password)

old_shout_id = 21458433

while True:
    try:
        print(account.like_shout(old_shout_id))
    except Exception:
        traceback.print_exc()

    time.sleep(3)

    try:
        print(account.unlike_shout(old_shout_id))
    except Exception:
        traceback.print_exc()

    time.sleep(3)
