from requests_oauthlib import OAuth1

class MyOAuth1(object):
    def __init__(self,
            client_key, client_secret,
            resource_owner_key, resource_owner_secret):

        self._ck = client_key
        self._cs = client_secret

        self._rok = resource_owner_key
        self._ros = resource_owner_secret

    def auth(self, signature_type):
        return OAuth1(client_key = self._ck,
                client_secret = self._cs,
                resource_owner_key = self._rok,
                resource_owner_secret = self._ros,
                signature_type = signature_type)
