from oauth import MyOAuth1
from oauth.tokens import get_tokens, UserOrPasswordInvalid

from taringa.api import TaringaAPI

class Taringa(object):
    def __init__(self, username, password):
        client_key, client_secret, res_owner_key, res_owner_secret = get_tokens(username, password)
        my_oauth = MyOAuth1(client_key, client_secret, res_owner_key, res_owner_secret)
        self._api = TaringaAPI(my_oauth)

    def get_my_info(self):
        return self._api.get_my_info()

    def get_user_info(self, nickname):
        return self._api.get_user_info(nickname)

    def follow(self, user_id):
        return self._api.follow(user_id)

    def like_shout(self, shout_id):
        return self._api.like_shout(shout_id)

    def unlike_shout(self, shout_id):
        return self._api.unlike_shout(shout_id)

    def get_followings(self, user_id):
        followings = []

        i = 1
        json = self._api.get_followings(user_id, i)

        while json:
            followings += json
            i += 1
            json = self._api.get_followings(user_id, i)

        return followings
